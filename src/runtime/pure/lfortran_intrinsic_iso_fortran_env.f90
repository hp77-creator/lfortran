module lfortran_intrinsic_iso_fortran_env
implicit none

integer, parameter :: int8 = -1
integer, parameter :: int16 = -1
integer, parameter :: int32 = 4
integer, parameter :: int64 = 8
integer, parameter :: real32 = 4
integer, parameter :: real64 = 8
integer, parameter :: real128 = -1

end module
