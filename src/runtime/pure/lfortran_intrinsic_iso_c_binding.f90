module lfortran_intrinsic_iso_c_binding
implicit none

integer, parameter :: c_int = 4
integer, parameter :: c_long = 4
integer, parameter :: c_long_long = 8
integer, parameter :: c_float = 4
integer, parameter :: c_double = 8

end module
